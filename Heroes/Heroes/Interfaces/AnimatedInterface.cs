﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Heroes.Interfaces
{
    interface AnimatedInterface
    {
        void animate(Services.AnimationService animationService);
    }
}
