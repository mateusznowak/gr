﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Heroes.Views._2D
{
    class NewGameView : Interfaces.ViewInterface
    {
        Texture2D backgroundTexture, arrowTexture;
        SpriteFont font, infoFont;
        Game1 parentGame;

        Models.Hero currentHero;
        List<Models.Hero> heroes;

        public NewGameView(Game parentGame)
        {
            this.parentGame = (Game1) parentGame;

            backgroundTexture = parentGame.Content.Load<Texture2D>("Intro/bg-select-hero");
            arrowTexture = parentGame.Content.Load<Texture2D>("Intro/arrow");
            font = parentGame.Content.Load<SpriteFont>("Fonts/IntroFont");
            infoFont = parentGame.Content.Load<SpriteFont>("Fonts/InfoIntroFont");

            heroes = (parentGame as Game1).getHeroRepository().findAll();
            currentHero = heroes[0];
        }

        public void Draw(GameTime gameTime)
        {
            MouseState ms = Mouse.GetState();
            
            Rectangle rectangle = new Rectangle(0, 0, parentGame.GraphicsDevice.Viewport.Width, parentGame.GraphicsDevice.Viewport.Height);

            (parentGame as Game1).getSpriteBatch().Draw(backgroundTexture, rectangle, Color.White);

            Rectangle arrowRectangle = new Rectangle(
                parentGame.getDeviceResolutionManager().computeWidth(85), parentGame.getDeviceResolutionManager().computeHeight(90),
                parentGame.getDeviceResolutionManager().computeWidth(15), parentGame.getDeviceResolutionManager().computeHeight(15)
            );

            if (arrowRectangle.Contains(ms.X, ms.Y))
            {
                if (ms.LeftButton == ButtonState.Pressed)
                {
                    (parentGame as Game1).getViewEventListener().trigger("game-view");
                    return;
                }
            }

            // Heroes
            Rectangle heroRectangle = new Rectangle(
                parentGame.getDeviceResolutionManager().computeWidth(10), parentGame.getDeviceResolutionManager().computeHeight(24),
                parentGame.getDeviceResolutionManager().computeWidth(34), parentGame.getDeviceResolutionManager().computeHeight(37)
            );

            int i = 0;
            foreach (Models.Hero hero in heroes)
            {
                Rectangle heroRect = new Rectangle(
                    parentGame.getDeviceResolutionManager().computeWidth(10) + (i * parentGame.getDeviceResolutionManager().computeWidth(11)), parentGame.getDeviceResolutionManager().computeHeight(65),
                    parentGame.getDeviceResolutionManager().computeWidth(10), parentGame.getDeviceResolutionManager().computeWidth(10)
                );

                if (heroRect.Contains(ms.X, ms.Y))
                {
                    (parentGame as Game1).getSpriteBatch().Draw(hero.texture, heroRect, Color.Violet);

                    if (ms.LeftButton == ButtonState.Pressed)
                    {
                        currentHero = hero;
                    }
                }
                else
                {
                    (parentGame as Game1).getSpriteBatch().Draw(hero.texture, heroRect, Color.White);
                }

                ++i;
            }

            (parentGame as Game1).getSpriteBatch().Draw(currentHero.texture, heroRectangle, Color.White);

            Vector2 heroNameVector = new Vector2(parentGame.getDeviceResolutionManager().computeWidth(48), parentGame.getDeviceResolutionManager().computeHeight(25));
            Vector2 heroMaxWarriors = new Vector2(parentGame.getDeviceResolutionManager().computeWidth(48), parentGame.getDeviceResolutionManager().computeHeight(32));

            (parentGame as Game1).getSpriteBatch().DrawString(font, currentHero.name,
                   heroNameVector,
                   Color.White
               );
            (parentGame as Game1).getSpriteBatch().DrawString(infoFont, String.Format("Maksymalna ilosc wojownikow: {0}", currentHero.maxWarriorCount),
                   heroMaxWarriors,
                   Color.White
               );

            // Back Button

            Vector2 backBtnVector = new Vector2(parentGame.getDeviceResolutionManager().computeWidth(90), parentGame.getDeviceResolutionManager().computeHeight(2));
            Rectangle backBtnRect = new Rectangle(parentGame.getDeviceResolutionManager().computeWidth(90), 0, 500, parentGame.getDeviceResolutionManager().computeHeight(6));

            if (backBtnRect.Contains(ms.X, ms.Y))
            {
                (parentGame as Game1).getSpriteBatch().DrawString(font, "Powrot",
                    backBtnVector,
                    Color.Orange
                );

                if (ms.LeftButton == ButtonState.Pressed)
                {
                    (parentGame as Game1).getViewEventListener().trigger("intro-view");
                    return;
                }
            }
            else
            {
                (parentGame as Game1).getSpriteBatch().DrawString(font, "Powrot",
                    backBtnVector,
                    Color.White
                );

            }
        }
    }
}
