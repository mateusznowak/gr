﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Heroes.Views._2D
{
    class IntroView : Interfaces.ViewInterface
    {
        Texture2D backgroundTexture, gameMenuTexture;
        SpriteFont introFont;

        Game parentGame;

        public IntroView(Game parentGame)
        {
            this.parentGame = parentGame;

            backgroundTexture = parentGame.Content.Load<Texture2D>("Intro/bg-intro");
            gameMenuTexture = parentGame.Content.Load<Texture2D>("Intro/game-menu");
            introFont = parentGame.Content.Load<SpriteFont>("Fonts/IntroFont");
        }

        public void Draw(GameTime gameTime)
        {
            MouseState ms = Mouse.GetState();

            int centerPosition = (parentGame as Game1).getDeviceResolutionManager().computeWidth(40);

            Rectangle rectangle = new Rectangle(0, 0, parentGame.GraphicsDevice.Viewport.Width, parentGame.GraphicsDevice.Viewport.Height);

            Rectangle newGameRectangle = new Rectangle(
                centerPosition,
                (parentGame as Game1).getDeviceResolutionManager().computeWidth(20),
                (parentGame as Game1).getDeviceResolutionManager().computeWidth(20), 
                (parentGame as Game1).getDeviceResolutionManager().computeHeight(8)
            );

            Rectangle newGameRectangleArea = new Rectangle(0, 0, 400, 80);

            (parentGame as Game1).getSpriteBatch().Draw(backgroundTexture, rectangle, Color.White);

            if (newGameRectangle.Contains(ms.X, ms.Y))
            {
                (parentGame as Game1).getSpriteBatch().Draw(gameMenuTexture, newGameRectangle, newGameRectangleArea, Color.Orange);
                if (ms.LeftButton == ButtonState.Pressed)
                {
                    (parentGame as Game1).getViewEventListener().trigger("new-game-view");
                    return;
                }
            }
            else
            {
                (parentGame as Game1).getSpriteBatch().Draw(gameMenuTexture, newGameRectangle, newGameRectangleArea, Color.White);
            }

            Rectangle loadGameRectangleArea = new Rectangle(0, 80, 400, 80);
            Rectangle loadGameRectangle = new Rectangle(
                centerPosition,
                (parentGame as Game1).getDeviceResolutionManager().computeWidth(30),
                (parentGame as Game1).getDeviceResolutionManager().computeWidth(20),
                (parentGame as Game1).getDeviceResolutionManager().computeHeight(8)
            );

            if (loadGameRectangle.Contains(ms.X, ms.Y))
            {
                (parentGame as Game1).getSpriteBatch().Draw(gameMenuTexture, loadGameRectangle, loadGameRectangleArea, Color.Orange);
            }
            else
            {
                (parentGame as Game1).getSpriteBatch().Draw(gameMenuTexture, loadGameRectangle, loadGameRectangleArea, Color.White);
            }

            Rectangle exitGameRectangleArea = new Rectangle(0, 160, 400, 80);
            Rectangle exitGameRectangle = new Rectangle(
                centerPosition,
                (parentGame as Game1).getDeviceResolutionManager().computeWidth(40),
                (parentGame as Game1).getDeviceResolutionManager().computeWidth(20),
                (parentGame as Game1).getDeviceResolutionManager().computeHeight(8)
            );

            if (exitGameRectangle.Contains(ms.X, ms.Y))
            {
                (parentGame as Game1).getSpriteBatch().Draw(gameMenuTexture, exitGameRectangle, exitGameRectangleArea, Color.Orange);
                if (ms.LeftButton == ButtonState.Pressed)
                {
                    parentGame.Exit();
                }
            }
            else
            {
                (parentGame as Game1).getSpriteBatch().Draw(gameMenuTexture, exitGameRectangle, exitGameRectangleArea, Color.White);
            }
        }
    }
}
