﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Heroes.Views._2D
{
    class TheGameView : Interfaces.ViewInterface
    {
        Game1 parentGame;
        Texture2D earthBackground, castleBackground;
        ObjectValue.Player player;

        public TheGameView(Game parentGame)
        {
            this.parentGame = (Game1) parentGame;
            Rectangle playerRectangle = new Rectangle(
                this.parentGame.getDeviceResolutionManager().computeWidth(45), this.parentGame.getDeviceResolutionManager().computeHeight(70), 
                this.parentGame.getDeviceResolutionManager().computeWidth(10), this.parentGame.getDeviceResolutionManager().computeHeight(15)
            );
            Rectangle playerAreaRectangle = new Rectangle(0, 0, 75, 75);
            Services.AnimationService animation = new Services.AnimationService(parentGame.Content.Load<Texture2D>("Game/dragon"), playerRectangle, playerAreaRectangle, 100f);
            this.player = new ObjectValue.Player(animation);
            animation.addToDelegate(player.animate);
            earthBackground = parentGame.Content.Load<Texture2D>("Game/earth");
            castleBackground = parentGame.Content.Load<Texture2D>("Game/castle");
        }

        public void Draw(GameTime gameTime)
        {
            MouseState ms = Mouse.GetState();
            KeyboardState ks = Keyboard.GetState();

            Rectangle rectangle = new Rectangle(0, 0, parentGame.GraphicsDevice.Viewport.Width, parentGame.GraphicsDevice.Viewport.Height);

            (parentGame as Game1).getSpriteBatch().Draw(earthBackground, rectangle, Color.White);
            (parentGame as Game1).getSpriteBatch().Draw(castleBackground, new Rectangle(
                this.parentGame.getDeviceResolutionManager().computeWidth(45), this.parentGame.getDeviceResolutionManager().computeHeight(40),
                this.parentGame.getDeviceResolutionManager().computeWidth(15), this.parentGame.getDeviceResolutionManager().computeHeight(25)
            ), Color.White);
            (parentGame as Game1).getSpriteBatch().Draw(player.animatedPlayer.texture, player.animatedPlayer.rectangle, player.animatedPlayer.playerAreaRectangle, Color.White, MathHelper.ToRadians(player.angle), new Vector2(0, 0), SpriteEffects.None, 0.0f);

            if (ks.IsKeyDown(Keys.Left))
            {
                player.moveLeft();
            }

            if (ks.IsKeyDown(Keys.Right))
            {
                player.moveRight();
            }

            if (ks.IsKeyDown(Keys.Up))
            {
                player.moveUp();
            }

            if (ks.IsKeyDown(Keys.Down))
            {
                player.moveDown();
            }

            player.animatedPlayer.tick(gameTime);
            
        }
    }
}
