﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace Heroes.Models
{
    public class Hero
    {
        public string name;
        public int maxWarriorCount;
        public Texture2D texture;

        public Hero(string name, int maxWarriorCount, Texture2D texture)
        {
            this.name = name;
            this.maxWarriorCount = maxWarriorCount;
            this.texture = texture;
        }
    }
}
