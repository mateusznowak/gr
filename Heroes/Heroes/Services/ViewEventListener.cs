﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Heroes.Services
{
    public class ViewEventListener
    {
        Game game;
        string currentTriggeredView = "intro-view";

        Dictionary<string, Interfaces.ViewInterface> viewList = new Dictionary<string, Interfaces.ViewInterface>();

        public ViewEventListener(Game game)
        {
            this.game = game;

            /**
             * Add all the views to the list
             */
            viewList.Add("intro-view", new Views._2D.IntroView(game));
            viewList.Add("new-game-view", new Views._2D.NewGameView(game));
            viewList.Add("game-view", new Views._2D.TheGameView(game));
        }

        public void dispatch(GameTime gameTime)
        {
            viewList[currentTriggeredView].Draw(gameTime);
        }

        public void trigger(string eventName)
        {
            if (viewList[eventName] != null)
            {
                this.currentTriggeredView = eventName;
            }
        }
    }
}
